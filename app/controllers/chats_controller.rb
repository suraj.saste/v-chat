class ChatsController < ApplicationController
  before_action :authorize_user

  def index
    @message = Message.new
    @messages = Message.custom_display
    @users = User.all
  end
end
